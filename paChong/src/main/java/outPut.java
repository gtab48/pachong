import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class outPut {
    public String JsonParser(String text) throws IOException {

        //过滤读出的utf-8前三个标签字节,从{开始读取
        JSONObject object = new JSONObject(text);

        JSONObject data = object.getJSONObject("data");

        JSONArray arr = data.getJSONArray("list");
        String dataJson = "";
        for(int i = 0; i < arr.length();i++){
            JSONObject jsonData = new JSONObject();
            JSONObject arrJson = arr.getJSONObject(i);
            jsonData.put("文章标题",arrJson.getString("post_title"));
            jsonData.put("文章地址",arrJson.getString("url"));
            jsonData.put("发布时间",arrJson.getString("post_date"));
            jsonData.put("发布用户名称",arrJson.getString("username"));
            dataJson += jsonData.toString()+",\n";
        }
        return  dataJson;
    }
}
